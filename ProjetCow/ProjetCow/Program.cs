﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ProjetCow
{
    class Program
    {
        public static int nombrepillier;
        private static int pilier;

        //Fonction pour calculer l'air d'un polygone regulier
        public static float Air(float[] X, float[] Y)
        {

            double area = 0.0;
            int j = nombrepillier - 1;

            for (int i = 0; i < nombrepillier; i++)
            {
                area += (X[j] * Y[i]) - (X[i] * Y[j]);


                //retour au premier case du tableau 
                j = i;
            }

            return (float)Math.Abs(area / 2.0);
        }

        //Fonction pour calculer la Graviter des point sur l'axe des abscisses 
        public static float GraviteX(float[] X, float[] Y)
        {
            double gravite = 0.0;
            int j = nombrepillier - 1;

            for (int i = 0; i < nombrepillier; i++)
            {
                gravite += (X[j] + X[i]) * ((X[j] * Y[i]) - (X[i] * Y[j]));
                j = i;
            }

            return (float)Math.Abs(gravite / (6.0 * Air(X, Y)));
        }



        //Fonction pour calculer la Graviter des point sur l'axe des ordonnées  
        public static float GraviteY(float[] X, float[] Y)
        {
            double gravite = 0.0;
            int x_1 = nombrepillier - 1;

            for (int i = 0; i < nombrepillier; i++)
            {
                gravite += (Y[x_1] + Y[i]) * ((X[x_1] * Y[i]) - (X[i] * Y[x_1]));
                x_1 = i;
            }
            return (float)Math.Abs(gravite / (6.0 * Air(X, Y)));
        }
        public static float CalculNumerateur(float[] X, float[] Y, int position)
        {
            float resultat;
            resultat = (X[position] * X[position + 1]) + (Y[position] * Y[position + 1]);
            return resultat;
        }
        public static double CalculDenominateur(float[] X, float[] Y, int position)
        {
            double resultat;
            resultat = CalculNorme(X, Y,position) * CalculNorme(X,Y,position + 1);
            return resultat;
        }
        public static double CalculThetai(float[] X, float[] Y, int position)
        {
            double resultat;
            resultat = Math.Acos(CalculNumerateur(X, Y, position+1) / CalculDenominateur(X, Y, position+1));
            return resultat;
        }
        public static double CalculNorme(float[] X, float[] Y, int position)
        {
            double resultat;
            resultat=Math.Sqrt((X[position] - X[position-1]) * (X[position] - X[position-1]));
            return resultat;
        }



        //Fonction pour savoir si la vaches est dans l'enclos
        public static bool AppartenancePointPolygone(float[] X, float[] Y, float[] gravite)
        {
            double Somme;
            double thetai;
             int i;
            Somme = 0;
            for (i = 0; i <= nombrepillier - 1; i++)
            {
                thetai = CalculThetai(X, Y, i);//fonction de calcul
                Somme = Somme + thetai;
            }
            if (Somme == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Nombre de piqué ? ");
                nombrepillier = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Il faut choissir un nombre positif");
            }
            //Verification pour ne pas dépasser 51 pillier max 
            if (nombrepillier > 50 || nombrepillier <= 0)
            {
                if (nombrepillier > 50)
                {
                    Console.WriteLine("Nombre de pillier supérieur à 50");
                }
                else
                {
                    Console.WriteLine("Nombre de pillier inférieur à 0");
                }

            }
            else
            {

                //Declaration de tableaux pour l'axe des abscisses et des ordonnées 
                float[] X = new float[nombrepillier];
                float[] Y = new float[nombrepillier];
                float[] gravite = new float[2];
                Console.WriteLine("Le nombre de piqué choissi est de  : " + nombrepillier);
                try
                {
                    for (int i = 0; i < nombrepillier; i++)
                    {
                        pilier++;
                        //Incrementation du nombre de pillier 
                        Console.WriteLine("Pilier " + pilier);

                        //Insertion des coordonnée des pilleirs par l'utilisateur  
                        Console.WriteLine("Position X");
                        X[i] = float.Parse(Console.ReadLine());

                        Console.WriteLine("Position Y");
                        Y[i] = float.Parse(Console.ReadLine());
                    }

                }
                catch
                {
                    Console.WriteLine("Il faut mettre une virgule au lieu d'un point ");
                }
                gravite[0] = GraviteX(X, Y);
                gravite[1] = GraviteY(X, Y);
                Console.WriteLine("L'air est de " + Air(X, Y));
                Console.WriteLine("Centre de gravité :" + gravite[0] + " , " + gravite[1]);
                if (AppartenancePointPolygone(X, Y, gravite) == true)
                {
                    Console.WriteLine("La vache est dans le pré");
                }
                else
                {
                    Console.WriteLine("Attention, la vache est hors du pré");
                }
            }

            Console.ReadLine();
        }
    }
}